//
//  AudioModel.m
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import "AudioModel.h"

@implementation AudioModel

+ (instancetype)sharedInstance
{
    static AudioModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AudioModel alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self loadUserTracks];
    }
    return self;
}

- (void)loadUserTracks
{
    NSURL *audioFolderURL = [[NSBundle mainBundle] URLForResource:@"Falling in reverce" withExtension:@""];
    
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *audioURLs = [fileManager contentsOfDirectoryAtURL:audioFolderURL
                                    includingPropertiesForKeys:nil
                                                       options:NSDirectoryEnumerationSkipsSubdirectoryDescendants
                                                         error:&error];
    NSMutableArray *audioTracks = [@[] mutableCopy];
    for (NSURL *url in audioURLs) {
        [audioTracks addObject:[[Track alloc] initWithURL:url]];
    }
    self.audioTracks = audioTracks;
}

+ (NSArray *)audioTracks
{
    return [[self sharedInstance] audioTracks];
}

@end
