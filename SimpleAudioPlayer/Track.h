//
//  Track.h
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Track : NSObject

@property (strong, readonly, nonatomic) NSString *title;
@property (strong, readonly, nonatomic) NSString *artist;
@property (strong, readonly, nonatomic) NSURL *URL;

- (id)initWithURL:(NSURL *)url;

@end
