//
//  TrackCell.h
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Track.h"

@interface TrackCell : UITableViewCell

@property (strong, nonatomic) Track *audioTrack;

@end
