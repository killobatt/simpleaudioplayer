//
//  TrackCell.m
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import "TrackCell.h"

@interface TrackCell ()

@property (weak, nonatomic) IBOutlet UILabel *trackNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@end

@implementation TrackCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self updateUI];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    [self updateUI];
    // Configure the view for the selected state
}

- (void)setAudioTrack:(Track *)audioTrack
{
    _audioTrack = audioTrack;
    [self updateUI];
}

- (void)updateUI
{
    self.trackNameLabel.text = self.audioTrack.title;
    self.artistNameLabel.text = self.audioTrack.artist;
}

@end
