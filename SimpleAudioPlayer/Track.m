//
//  Track.m
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import "Track.h"

@interface Track ()

@property (strong, readwrite, nonatomic) NSString *title;
@property (strong, readwrite, nonatomic) NSString *artist;
@property (strong, readwrite, nonatomic) NSURL *URL;

@end

@implementation Track

- (id)initWithURL:(NSURL *)url
{
    self = [super init];
    if (self) {
        self.URL = url;
        self.title = self.URL.lastPathComponent;
        self.artist = [[self.URL URLByDeletingLastPathComponent] lastPathComponent];
    }
    return self;
}

@end
