//
//  PlaybackController.h
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"
@import AVFoundation;

@interface PlaybackController : NSObject

// entrance
- (void)startPlaybackForTracks:(NSArray *)tracks firstTrackIndex:(NSInteger)firstTrackIndex;

// actions
- (void)togglePlayPause;
- (void)playNextTrack;
- (void)playPreviousTrack;

// parameters
@property (assign, nonatomic) BOOL random;
@property (assign, nonatomic) BOOL repeat;

// exit
@property (strong, readonly, nonatomic) AVAudioPlayer *player;

// state
@property (strong, readonly, nonatomic) NSArray *tracks;
@property (assign, readonly, nonatomic) NSInteger currentTrack;

@end
