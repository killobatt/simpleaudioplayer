//
//  ViewController.h
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerController : UIViewController

@property (assign, nonatomic) NSInteger trackNumber;

@end
