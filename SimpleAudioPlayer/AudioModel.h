//
//  AudioModel.h
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"

@interface AudioModel : NSObject

@property (strong, nonatomic) NSArray *audioTracks;

+ (instancetype)sharedInstance;
+ (NSArray *)audioTracks;

@end
