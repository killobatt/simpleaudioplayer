//
//  PlaybackController.m
//  SimpleAudioPlayer
//
//  Created by Vjacheslav Volodjko on 09.11.13.
//  Copyright (c) 2013 me. All rights reserved.
//

#import "PlaybackController.h"

@interface PlaybackController ()< AVAudioPlayerDelegate >

@property (strong, readwrite, nonatomic) NSArray *tracks;
@property (strong, readwrite, nonatomic) AVAudioPlayer *player;
@property (assign, readwrite, nonatomic) NSInteger currentTrack;

@end

@implementation PlaybackController


#pragma mark - Actions 

- (void)startPlaybackForTracks:(NSArray *)tracks firstTrackIndex:(NSInteger)firstTrackIndex
{
    self.currentTrack = firstTrackIndex;
    self.tracks = tracks;
    [self startPlayback];
}

- (void)startPlayback
{
    NSError *error = nil;
    Track *track = self.tracks[self.currentTrack];
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:track.URL
                                                         error:&error];
    self.player.delegate = self;
    if (error) {
        NSLog(@"Error: %@", error);
    } else {
        [self.player play];
    }
}

- (void)togglePlayPause
{
    
}

- (void)playNextTrack
{
    self.currentTrack ++;
    [self startPlayback];
}

- (void)playPreviousTrack
{
    self.currentTrack --;
    [self startPlayback];
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    // play next track;
}

/* if an error occurs while decoding it will be reported to the delegate. */
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    // at least log;
}

#if TARGET_OS_IPHONE

/* audioPlayerBeginInterruption: is called when the audio session has been interrupted while the player was playing. The player will have been paused. */
- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player;

/* audioPlayerEndInterruption:withOptions: is called when the audio session interruption has ended and this player had been interrupted while playing. */
/* Currently the only flag is AVAudioSessionInterruptionFlags_ShouldResume. */
- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags NS_AVAILABLE_IOS(6_0);

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withFlags:(NSUInteger)flags NS_DEPRECATED_IOS(4_0, 6_0);

/* audioPlayerEndInterruption: is called when the preferred method, audioPlayerEndInterruption:withFlags:, is not implemented. */
- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player NS_DEPRECATED_IOS(2_2, 6_0);

@end
